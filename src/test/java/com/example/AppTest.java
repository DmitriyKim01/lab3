package com.example;

import static org.junit.Assert.*;

import org.junit.Test;


/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testEquals(){
        App app = new App();
        assertEquals("Testing echo method", 5, app.echo(5));
    }

    @Test
    public void testOneMore(){
        App app = new App();
        assertEquals("Testing oneMore method", 5 , app.oneMore(4));
    }
}
